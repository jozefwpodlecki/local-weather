import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations'; 
import { MainComponent } from './components/Main/Main.component';
import { HttpClientModule } from '@angular/common/http';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { FooterComponent } from './components/Footer/Footer.component';
import { SpinnerComponent } from './components/Spinner/Spinner.component';

@NgModule({
    imports: [
        BrowserModule,
        HttpClientModule,
        BrowserAnimationsModule,
        FontAwesomeModule
    ],
    bootstrap: [
        MainComponent
    ],
    declarations: [
        MainComponent,
        SpinnerComponent,
        FooterComponent
    ]
})

export class AppModule {
}
