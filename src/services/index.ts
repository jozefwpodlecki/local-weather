import { InjectionToken, inject } from '@angular/core';
import { environment } from 'src/environments/environment';
import { WeatherService } from './WeatherService/WeatherService';
import { MockWeatherService } from './WeatherService/MockWeatherService';
import { HttpClient } from '@angular/common/http';

export const WeatherServiceToken = new InjectionToken (
    "WeatherService",
    {
        providedIn: "root",
        factory: () => {

            if(environment.production) {
                return new WeatherService(inject(HttpClient))
            }

            return new MockWeatherService();
        }
    }
);