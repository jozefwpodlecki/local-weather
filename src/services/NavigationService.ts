import { Injectable } from '@angular/core';
import { GeographicCoordinates } from 'src/models/GeographicCoordinates';

@Injectable({
  providedIn: 'root'
})

export class NavigationService {

    constructor() { }

    GetPosition() {
        return new Promise<GeographicCoordinates>((resolve, reject) => {
            if (navigator.geolocation) {
                navigator.geolocation.getCurrentPosition((position) => {
                    const { latitude, longitude } = position.coords;
        
                    resolve({
                        latitude,
                        longitude
                    })
                });
                return;
            }

            reject();
        })
    }
}