export interface WeatherDataList {
    cod: string,
    message: number,
    cnt: number,
    list: WeatherData[],
    city: {
        id: number,
        name: string,
        coord: {
            lat: number,
            lon: number
        },
        country: string
    },
    id: number,
    name: string
    coord: {
        lon: number,
        lat: number
    },
    country: string
}

export interface WeatherData {
    coord: {
        lon: number,
        lat: number
    },
    weather: {
        id: number,
        main: string,
        description: string,
        icon: string
    }[]
    base: string,
    main: {
        temp: number,
        pressure: number,
        humidity: number,
        feels_like: number,
        temp_min: number,
        temp_max: number,
        visibility: number,
        sea_level?: number
    },
    wind: {
        speed: number,
        deg: number,
    },
    clouds: {
        all: number
    },
    dt: number,
    dt_txt?: string,
    sys: {
        type: number,
        id: number,
        message: number,
        country: string,
        sunrise: number,
        sunset: number
    },
    id: number,
    name: string,
    cod: number
}