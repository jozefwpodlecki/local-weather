import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, throwError, from } from 'rxjs';
import { retry, catchError } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { WeatherData, WeatherDataList } from './WeatherData';
import { IWeatherService } from './IWeatherService';

@Injectable({
    providedIn: 'root'
})
export class WeatherService implements IWeatherService {

    baseUrl: string;

    constructor(private _http: HttpClient) { 
        this.baseUrl = "https://api.openweathermap.org";
    }

    httpOptions = {
        headers: new HttpHeaders({
            'Content-Type': 'application/json',
            'Access-Control-Allow-Origin': '*',
            'Access-Control-Allow-Credentials': 'true',
            'Access-Control-Allow-Methods': 'GET, POST'
        })
    }

    GetForecast({ latitude, longitude }) {
        const url = `${this.baseUrl}/data/2.5/forecast?lat=${latitude}&lon=${longitude}&appid=${environment.weatherApiKey}`;
        
        return from(fetch(url)
            .then(response => response.json()))

        // return this._http
        //     .get<WeatherDataList>(url, this.httpOptions)
    }

    GetWeather({ latitude, longitude }) {
        const url = `${this.baseUrl}/data/2.5/weather?lat=${latitude}&lon=${longitude}&appid=${environment.weatherApiKey}`;
        
        return from(fetch(url)
            .then(response => response.json()))

        // return this._http
        //     .get<WeatherData>(url, this.httpOptions)
    }
}