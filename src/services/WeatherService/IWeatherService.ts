import { Observable } from 'rxjs';
import { WeatherData, WeatherDataList } from './WeatherData';
import { GeographicCoordinates } from 'src/models/GeographicCoordinates';

export interface IWeatherService {
    
    GetForecast(coordinates: GeographicCoordinates) : Observable<WeatherDataList>

    GetWeather(coordinates: GeographicCoordinates) : Observable<WeatherData>
}