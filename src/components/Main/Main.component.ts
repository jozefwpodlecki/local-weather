import { Component, OnInit, Inject } from '@angular/core';
import { trigger, style, animate, transition } from '@angular/animations';
import { NavigationService } from 'src/services/NavigationService';
import { faThermometerHalf, faTemperatureHigh, faTemperatureLow, faCloudRain, faSnowflake, faCloudSun, faCloud, faTint, faWind, faSun } from '@fortawesome/free-solid-svg-icons';
import { IWeatherService } from 'src/services/WeatherService/IWeatherService';
import { WeatherServiceToken } from 'src/services';
import * as moment from 'moment'

@Component({
    selector: 'app',
    animations: [
        trigger(
            'enterAnimation', [
                transition(':enter', [
                    style({opacity: 0}),
                    animate('500ms', style({transform: 'translateX(0)', opacity: 1}))
                ]),
                transition(':leave', [
                    style({opacity: 1}),
                    animate('500ms', style({transform: 'translateX(100%)', opacity: 0}))
                ])
            ]
        )
    ],
    templateUrl: `./Main.component.html`,
    styleUrls: ['./Main.component.scss']
})

export class MainComponent implements OnInit {
    faCloud = faCloud;
    faWind = faWind;
    faCloudRain = faCloudRain;
    faSnowflake = faSnowflake;
    faCloudSun = faCloudSun;
    faSun = faSun;
    faTint = faTint;
    faThermometerHalf = faThermometerHalf;
    faTemperatureHigh = faTemperatureHigh;
    faTemperatureLow = faTemperatureLow;
    temperature: string;
    description: string;
    place: string;
    country: string;
    pressure: number;
    humidity: number;
    visibility: string
    wind: {
        speed: number,
        deg: number
    } = {
        speed: null,
        deg: null
    }
    year: number;
    forecasts: any[]
    loading: boolean;

    constructor(
        @Inject(WeatherServiceToken)
        private _weatherService: IWeatherService,
        private _navigationService: NavigationService) {
            this.forecasts = []
            this.loading = true;
    }

    kelvinToCelcius(kelvin: number) {
        return (kelvin - 273.15).toFixed(0)
    }

    ngOnInit(): void {
        
        this._navigationService.GetPosition().then(position => {
            this._weatherService.GetForecast(position).subscribe(data => {
                
                this.forecasts = data.list.map(pr => {
                    const [weather] = pr.weather;
                    const { description } = weather;
                    let icon = faSun;
                    let day = moment(pr.dt_txt).format('ddd h:mm');
                    let temperature = this.kelvinToCelcius(pr.main.temp)

                    if(description.includes('rain')) {
                        icon = faCloudRain;
                    }

                    if(description.includes('snow')) {
                        icon = faSnowflake;
                    }

                    if(description.includes('cloud')) {
                        icon = faCloud;
                    }

                    return {
                        day,
                        temperature,
                        icon
                    }
                }).slice(0, 5);
            })

            this._weatherService.GetWeather(position).subscribe(data => {
                this.place = data.name
                this.country = data.sys.country
                this.description
                this.temperature = this.kelvinToCelcius(data.main.temp);
                this.pressure = data.main.pressure
                this.humidity = data.main.humidity
                this.wind = data.wind;
                this.loading = false;
            })
        })
        
    }
}