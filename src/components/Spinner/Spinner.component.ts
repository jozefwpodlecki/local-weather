import { Component, OnInit } from '@angular/core';
import { faLinkedinIn, faGithub } from '@fortawesome/free-brands-svg-icons';
import { faGlobe } from '@fortawesome/free-solid-svg-icons';

@Component({
    selector: 'spinner',
    templateUrl: `./Spinner.component.html`,
    styleUrls: ['./Spinner.component.scss']
})

export class SpinnerComponent {
    
    constructor() {}
}