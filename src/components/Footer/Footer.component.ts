import { Component, OnInit } from '@angular/core';
import { faLinkedinIn, faGithub } from '@fortawesome/free-brands-svg-icons';
import { faGlobe } from '@fortawesome/free-solid-svg-icons';

@Component({
    selector: 'site-footer',
    templateUrl: `./Footer.component.html`,
    styleUrls: ['./Footer.component.scss']
})

export class FooterComponent {
    faGlobe = faGlobe
    faGithub = faGithub
    faLinkedinIn = faLinkedinIn
    year: number;
    author: string;

    constructor() {
        this.year = new Date().getFullYear();
        this.author = "Józef Podlecki";
    }
}