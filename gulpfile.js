const gulp = require('gulp');
const replace = require('gulp-replace');
const del = require('del');

gulp.task('moveJs', function() {
    return gulp.src("templates/*.js")
        .pipe(gulp.dest('static'));    
})

gulp.task('moveCss', function() {
    return gulp.src("templates/*.css")
        .pipe(gulp.dest('static'));
})

gulp.task('deleteFiles', function() {
    return del(['templates/*.css', 'templates/*.js', 'templates/*.txt'])
})

gulp.task('replaceAndMoveHtml', function() {
    return gulp.src("templates/*.html")
        .pipe(replace(/src\s*=\s*"(.+?)"/g, `src="{{ url_for('static', filename='$1') }}"`))
        .pipe(replace(/link.*rel="stylesheet".*href\s*=\s*"(.+?)"/g, `<link rel="stylesheet" href="{{ url_for('static', filename='$1') }}"`))
        .pipe(gulp.dest('templates'));
})


gulp.task('default', gulp.series('replaceAndMoveHtml','moveJs','moveCss', 'deleteFiles'));